CC = g++
CFLAGS = -Wall -g -pedantic -std=c++11

build: clean command processor exchange product order main
	$(CC) $(CFLAGS) -o bin/main bin/*.o -lcurl -ljsoncpp

command: include/command.h src/command.cpp
	$(CC) $(CFLAGS) -o bin/command.o -c src/command.cpp

processor: include/order_processor.h include/default_order_processor.h src/default_order_processor.cpp
	$(CC) $(CFLAGS) -o bin/processor.o -c src/default_order_processor.cpp

http_api: include/http_api.h include/curl_api.h src/curl_api.cpp
	$(CC) $(CFLAGS) -o bin/http_api.o -c src/curl_api.cpp

exchange: http_api include/exchange.h include/ma_exchange.h src/ma_exchange.cpp
	$(CC) $(CFLAGS) -o bin/exchange.o -c src/ma_exchange.cpp

product: include/product.h src/product.cpp
	$(CC) $(CFLAGS) -o bin/product.o -c src/product.cpp

order: include/order.h src/order.cpp
	$(CC) $(CFLAGS) -o bin/order.o -c src/order.cpp

main: src/main.cpp include/product.h include/order.h
	$(CC) $(CFLAGS) -o bin/main.o -c src/main.cpp

.PHONY: clean

clean: 
	rm -rf bin/*
