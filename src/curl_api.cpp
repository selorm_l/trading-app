#include <stdexcept>
#include "../include/curl_api.h"

const std::string DEFAULT_URL = "https://exchange.matraining.com";

size_t CurlAPI::write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{
    static_cast<std::string*>(userp)->append(static_cast<char*>(buffer), size * nmemb);
    return size * nmemb;
}

CurlAPI::CurlAPI() : CurlAPI::CurlAPI(DEFAULT_URL) {}

CurlAPI::CurlAPI(const std::string& url)
{
    base_url = url;
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    if (!curl)
    {
        curl_global_cleanup();
        throw std::runtime_error {"failed to initialize curl"};
    }
}

CurlAPI::~CurlAPI() {
    curl_easy_cleanup(curl);
    curl_global_cleanup();
}

Json::Value CurlAPI::parse(const std::string& json_string)
{
    Json::Reader reader;
    Json::Value root;
    reader.parse(json_string, root);
    return root;
}

Json::Value CurlAPI::get(const std::string& url)
{
    std::string response_string {};
    std::string full_url {base_url + url};
    curl_easy_setopt(curl, CURLOPT_URL, full_url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
    auto res = curl_easy_perform(curl);
    if (res != CURLE_OK) 
    {    
        curl_easy_reset(curl);
        throw std::runtime_error {curl_easy_strerror(res)};
    }
    curl_easy_reset(curl);
    return parse(response_string);
}

Json::Value CurlAPI::post(const std::string& url, const Json::Value& payload)
{
    struct curl_slist* headers = nullptr;
    Json::FastWriter fast_writer;
    auto payload_string = fast_writer.write(payload);
    headers = curl_slist_append(headers, "Content-Type: application/json");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload_string);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, -1l);

    std::string response_string {};
    std::string full_url {base_url + url};
    curl_easy_setopt(curl, CURLOPT_URL, full_url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
    auto res = curl_easy_perform(curl);
    if (res != CURLE_OK) 
    {    
        curl_easy_reset(curl);
        throw std::runtime_error {curl_easy_strerror(res)};
    }
    curl_easy_reset(curl);
    return parse(response_string);
}