// Trading System
#include <iostream>
#include <exception>
#include "../include/exchange.h"
#include "../include/ma_exchange.h"
#include "../include/http_api.h"
#include "../include/curl_api.h"
#include "../include/command.h"
#include "../include/order_processor.h"
#include "../include/default_order_processor.h"
#include "../include/product.h"
#include "../include/order.h"

int main(int argc, char* argv[])
{
    std::string trade_key;
    if (argc > 2) trade_key = argv[1];
    try
    {
        // Setup exchange
        CurlAPI curl_api {};
        MAExchange ma_exchange {&curl_api, trade_key};
        Exchange* exchange {&ma_exchange};

        // Setup order processor
        DefaultOrderProcessor default_processor {};
        OrderProcessor* processor {&default_processor};

        std::cout << "Welcome to GreenNote Exchange!\n";

        int input;
        Command command;

        do
        {
            std::cout << "\nEnter a command:\n"
                    << static_cast<unsigned>(Command::BUY)            << " BUY\n"
                    << static_cast<unsigned>(Command::SELL)           << " SELL\n"
                    << static_cast<unsigned>(Command::LIST_ORDERS)    << " LIST ORDERS\n"
                    << static_cast<unsigned>(Command::LIST_PRODUCTS)  << " LIST PRODUCTS\n"
                    << static_cast<unsigned>(Command::EXIT)           << " EXIT\n"
                    << ": ";
            std::cin >> input;
            command = static_cast<Command>(input);

            switch (command)
            {
            case Command::BUY:
                buy_product(*exchange, *processor);
                break;

            case Command::SELL:
                sell_product(*exchange, *processor);
                break;

            case Command::LIST_ORDERS:
                list_orders(*exchange);
                break;

            case Command::LIST_PRODUCTS:
                list_products(*exchange);
                break;
            
            case Command::EXIT:
                std::cout << "See you later!" << std::endl;
            }
        } while (command != Command::EXIT);

        return 0;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}