// Stub implementation of Exchange interface
#include "../include/exchange_stub.h"

ExchangeStub::ExchangeStub()
{
    products.push_back(Product {"MSFT", 5000, 10000, 0.824, 0.405, 2, 1});
    products.push_back(Product {"NFLX", 5000, 10000, 1.5, 0.015625, 0.015625, 1});
    products.push_back(Product {"GOOGL", 5000, 10000, 2.0, 1.0, 1, 1});
    products.push_back(Product {"AAPL", 5000, 10000, 2.3, 2.2, 1, 1});
    products.push_back(Product {"TSLA", 5000, 10000, 0.6, 0.6, 1.3, 1});
    products.push_back(Product {"IBM", 5000, 10000, 1.5, 1.004, 1.004, 1});
    products.push_back(Product {"ORCL", 5000, 10000, 1.552, 1.552, 2, 1});
    products.push_back(Product {"AMZN", 5000, 10000, 1.7, 1.346, 1.5, 1});
}

std::vector<Product> ExchangeStub::get_products() const { return products; }

void ExchangeStub::create_order(const Order& order) { orders.push_back(order); }

std::vector<Order> ExchangeStub::get_open_orders() const { return orders; }