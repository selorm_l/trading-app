#include <iomanip>
#include "../include/order.h"

Order::Order(const std::string& pd, size_t q, double pr, const std::string& s)
    : product {pd}, quantity {q}, price {pr}, side {s}
{}

std::string Order::get_product() const { return product; }
size_t Order::get_quantity() const { return quantity; }
double Order::get_price() const { return price; }
std::string Order::get_side() const { return side; }

std::ostream& operator<<(std::ostream& stream, const Order& order)
{
    stream << std::setw(6) << std::left << order.get_product() << '('
        << std::setw(4) << order.get_side() << ' '
        << std::setw(5) << order.get_quantity() << ' '
        << std::setw(5) << std::fixed << std::setprecision(3)
        << order.get_price() << ')';

    return stream;
}