#include <iomanip>
#include "../include/product.h"

Product::Product(const std::string& t, size_t sl, size_t bl, double ap, double bp, double ltp, double mps)
    : ticker {t}, sell_limit {sl}, buy_limit {bl}, ask_price {ap}, bid_price {bp}, last_traded_price{ltp}, max_price_shift {mps}
{}

inline std::string Product::get_ticker() const { return ticker; }
inline size_t Product::get_sell_limit() const { return sell_limit; }
inline size_t Product::get_buy_limit() const { return buy_limit; }
inline double Product::get_ask_price() const { return ask_price; }
inline double Product::get_bid_price() const { return bid_price; }
inline double Product::get_last_traded_price() const { return last_traded_price; }
inline double Product::get_max_price_shift() const { return max_price_shift; }

std::ostream& operator<<(std::ostream& stream, const Product& product)
{
    stream << std::setw(6) << std::left << product.get_ticker() << '('
        << std::setw(5) << std::fixed << std::setprecision(3) << product.get_bid_price() << ", "
        << std::setw(5) << std::fixed << std::setprecision(3) << product.get_ask_price() << ')';

    return stream;
}