#include <iostream>
#include "../include/command.h"
#include "../include/order_processor.h"

/// place_order is used by buy_product and sell_product
/// to place an order on the exchange
void place_order(Command c, Exchange& exchange, OrderProcessor& processor)
{
    std::cout << "Enter the ticker: ";
    std::string ticker;
    std::cin >> ticker;

    std::cout << "Enter the price: ";
    double price;
    std::cin >> price;

    std::cout << "Enter the quantity: ";
    size_t quantity;
    std::cin >> quantity;

    std::string side = c == Command::BUY ? "BUY" : "SELL";

    Order order {ticker, quantity, price, side};

    processor.place_order(order, exchange);

    std::cout << "Order placed successfully!" << std::endl;
}

void buy_product(Exchange& exchange, OrderProcessor& processor)
{
    place_order(Command::BUY, exchange, processor);
}

void sell_product(Exchange& exchange, OrderProcessor& processor)
{
    place_order(Command::SELL, exchange, processor);
}

void list_orders(const Exchange& exchange)
{
    std::cout << "\nOpen orders:\n";
    for (const auto& order : exchange.get_open_orders())
        std::cout << order << '\n';
    std::cout << std::endl;
}

void list_products(const Exchange& exchange)
{
    std::cout << "\nProducts available:\n";
    for (const auto& product : exchange.get_products())
        std::cout << product << '\n';
    std::cout << std::endl;
}
