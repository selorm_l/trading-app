#include "../include/ma_exchange.h"

std::vector<Product> MAExchange::get_products() const
{
    std::vector<Product> products;
    auto response = api->get("/pd");
    for (const auto& element : response)
    {
        products.push_back(Product {
            element.get("TICKER", "").asString(),
            element.get("SELL_LIMIT", 0).asLargestUInt(),
            element.get("BUY_LIMIT", 0).asLargestUInt(),
            element.get("ASK_PRICE", 0.0).asDouble(),
            element.get("BID_PRICE", 0.0).asDouble(),
            element.get("LAST_TRADED_PRICE", 0.0).asDouble(),
            element.get("MAX_PRICE_SHIFT", 0.0).asDouble()
        });
    }
    return products;
}

void MAExchange::create_order(const Order& order)
{
    Json::Value payload;
    payload["product"] = order.get_product();
    payload["quantity"] = order.get_quantity();
    payload["price"] = order.get_price();
    payload["side"] = order.get_side();
    const std::string url {'/' + trade_key + "/order"};
    api->post(url, payload);
}

std::vector<Order> MAExchange::get_open_orders() const
{
    std::vector<Order> orders;
    auto response = api->get("/orderbook");
    for (const auto& orderbook: response)
    {
        for (const auto& element : orderbook["fullOrderBook"])
        {
            if (!element["executions"][0])
                orders.push_back(Order {
                    element["product"].asString(),
                    element["quantity"].asLargestUInt(),
                    element["price"].asDouble(),
                    element["side"].asString()
                });
        }
    }
    return orders;
}