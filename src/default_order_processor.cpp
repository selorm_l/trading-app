#include "../include/default_order_processor.h"

void DefaultOrderProcessor::place_order(const Order& order, Exchange& exchange)
{
    exchange.create_order(order);
}