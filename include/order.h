#ifndef ORDER_H
#define ORDER_H

#include <string>
#include <ostream>

class Order
{
private:
    std::string product;
    size_t quantity;
    double price;
    std::string side;

public:
    // Constructors
    Order(const std::string& pd, size_t q, double pr, const std::string& s);
    Order() = default;

    // Accessors
    std::string get_product() const;
    size_t get_quantity() const;
    double get_price() const;
    std::string get_side() const;
};

std::ostream& operator<<(std::ostream& stream, const Order& order);

#endif // ORDER_H