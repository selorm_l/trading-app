/* 
    A simple order processor implementation
 */

#ifndef DEFAULT_ORDER_PROCESSOR_H
#define DEFAULT_ORDER_PROCESSOR_H

#include "order_processor.h"

class DefaultOrderProcessor : public OrderProcessor
{
public:
    DefaultOrderProcessor() = default;
    void place_order(const Order& order, Exchange& exchange) override;
};

#endif // DEFAULT_ORDER_PROCESSOR_H