#ifndef MA_EXCHANGE_H
#define MA_EXCHANGE_H

#include "exchange.h"
#include "http_api.h"

class MAExchange : public Exchange
{
private:
    HttpAPI* api;
    std::string trade_key;

public:
    MAExchange() = delete;
    MAExchange(HttpAPI* api, const std::string& key) : api {api}, trade_key {key} {};

    std::vector<Product> get_products() const override;
    void create_order(const Order& order) override;
    std::vector<Order> get_open_orders() const override;
};

#endif //MA_EXCHANGE_H