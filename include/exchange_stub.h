#ifndef EXCHANGE_STUB_H
#define EXCHANGE_STUB_H

#include "exchange.h"

class ExchangeStub : public Exchange
{
private:
    std::vector<Product> products;
    std::vector<Order> orders;

public:
    ExchangeStub();
    std::vector<Product> get_products() const override;
    void create_order(const Order& order) override;
    std::vector<Order> get_open_orders() const override;
};

#endif // EXCHANGE_STUB_H