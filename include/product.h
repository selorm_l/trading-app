#ifndef PRODUCT_H
#define PRODUCT_H

#include <string>
#include <ostream>

class Product
{
private:
    std::string ticker;
    size_t sell_limit;
    size_t buy_limit;
    double ask_price;
    double bid_price;
    double last_traded_price;
    double max_price_shift;

public:
    // Constructors
    Product(const std::string& t, size_t sl, size_t bl, double ap, double bp, double ltp, double mps);
    Product() = default;

    // Accessors
    std::string get_ticker() const;
    size_t get_sell_limit() const;
    size_t get_buy_limit() const;
    double get_ask_price() const;
    double get_bid_price() const;
    double get_last_traded_price() const;
    double get_max_price_shift() const;
};

std::ostream& operator<<(std::ostream& stream, const Product& product);

#endif // PRODUCT_H