/* 
    Order processor interface
 */

#ifndef ORDER_PROCESSOR_H
#define ORDER_PROCESSOR_H

#include "order.h"
#include "exchange.h"

class OrderProcessor
{
public:
    virtual ~OrderProcessor() = default;
    virtual void place_order(const Order& order, Exchange& exchange) = 0;
};

#endif