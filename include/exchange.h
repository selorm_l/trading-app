// Exchange interface
#ifndef EXCHANGE_H
#define EXCHANGE_H

#include <vector>
#include "../include/product.h"
#include "../include/order.h"

class Exchange
{
public:
    virtual ~Exchange() = default;
    virtual std::vector<Product> get_products() const = 0;
    virtual void create_order(const Order& order) = 0;
    virtual std::vector<Order> get_open_orders() const = 0;
};

#endif // EXCHANGE_H