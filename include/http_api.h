#ifndef HTTP_API_H
#define HTTP_API_H

#include <string>
#include <jsoncpp/json/json.h>

class HttpAPI
{
protected:
    std::string base_url;

public:
    virtual ~HttpAPI() = default;
    virtual Json::Value get(const std::string& url) = 0;
    virtual Json::Value post(const std::string& url, const Json::Value& payload) = 0;
};

#endif // HTTP_API_H