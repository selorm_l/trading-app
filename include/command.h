#ifndef COMMAND_H
#define COMMAND_H

#include "exchange.h"
#include "order_processor.h"

enum class Command
{
    BUY = 1,
    SELL,
    LIST_ORDERS,
    LIST_PRODUCTS,
    EXIT
};

// buy_product places a buy order for a product on the exchange
void buy_product(Exchange& exchange, OrderProcessor& processor);

// sell_product places a sell order for a product on the exchange
void sell_product(Exchange& exchange, OrderProcessor& processor);

// list_orders displays all open orders on the exchange
void list_orders(const Exchange& exchange);

// List all products available on the exchange
void list_products(const Exchange& exchange);

#endif