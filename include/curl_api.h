#ifndef CURL_API_H
#define CURL_API_H

#include <curl/curl.h>
#include "http_api.h"

class CurlAPI : public HttpAPI
{
private:
    CURL* curl;
    Json::Value parse(const std::string& json_string);
    static size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp);

public:
    CurlAPI();
    explicit CurlAPI(const std::string& base_url);

    ~CurlAPI();

    Json::Value get(const std::string& url);
    Json::Value post(const std::string& url, const Json::Value& payload);
};

#endif // CURL_API_H