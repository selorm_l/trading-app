# Demo Trading Application

This is a demo trading application that can
1. View exchange orderbook.
2. View products available on the exchange.
3. Buy and sell products available on an exchange.

It currently uses the Mallon Associates training exchange.

## Build and run the application

To build this application, you need to have `make` installed on your computer.

```
$ make                 # build the application
$ bin/main <trade-key> # run the application
```
